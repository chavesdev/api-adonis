'use strict'

const { test, trait } = use('Test/Suite')('Flood')
const Flood = use('App/Models/Flood')

trait('Test/ApiClient')

test('pegar lista de floods', async ({ client }) => {
  const response = await client.get('/api/v1/floods').end()

  response.assertStatus(404)

})

test('filtrar por localização com variaveis erradas', async({ client }) => {
  const response = await client.get('/api/v1/floods/ontem/hoje').end()

  response.assertStatus(200)
  response.assertJSONSubset([])
})

test('filtrar por localização com latitude e longitude', async({ client }) => {
  const response = await client.get('/api/v1/floods/-27.499325/-48.511365').end()

  response.assertStatus(200)
})
