'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
    Route.post('/register', 'AuthController.register')
    Route.post('/login','AuthController.login')
}).prefix('api/v1')

Route.group(() => {
    Route.get('/:latitude/:longitude/:radius?', 'FloodController.index')
    // Route.post('/','FloodController.store').validator('StoreFlood')
    Route.post('/','FloodController.store')
}).prefix('api/v1/floods')
//.middleware(['jwt'])

// Route.get('/:latitude/:longitude/:radius?', 'FloodController.index')
// Route.post('/','FloodController.store').prefix('api/v1/floods')
