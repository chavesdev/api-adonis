'use strict'

const Flood = use('App/Models/Flood');
const StoreFlood = use ('App/Validators/StoreFlood');
const { validate } = use('Validator')

class FloodController {

    async index ({request}) {
        const {latitude, longitude, radius} = request.params;
        const floods = await Flood.nearby(latitude, longitude, radius);
        return floods[0];
    }

    async store ({ request, response }) {
        const validation = await validate(request.all(), new StoreFlood().rules)

        if (validation.fails()) {
            return {errors : validation.messages()}
        }

        const {title, latitude, longitude} = request.all();
        const flood = await Flood.create({title : title, latitude : latitude, longitude : longitude});
        return flood;
    }
}

module.exports = FloodController
