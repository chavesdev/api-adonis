'use strict'

const User = use('App/Models/User');

class AuthController {

    async register({request}){
        const data = request.only(['first_name','last_name','email', 'password', 'latitude','longitude']);
        const user = await User.create(data);
        return user;
    }

    async login({request, auth}){
        const { email, password } = request.all();
        const token = await auth.attempt(email, password);

        return token;
    }
}

module.exports = AuthController
