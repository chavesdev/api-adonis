'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use('Database')

class Flood extends Model {

    static nearby(latitude, longitude, radius){
        if(radius == null){
            radius = 9;
        }
        var query = "select *, ( 6371 * acos( cos( radians(?) ) * cos( radians( latitude ) )* cos( radians( longitude ) - radians(?)) + sin( radians(?) ) * sin( radians( latitude ) ) )) as distance from floods having distance < ? order by distance ASC";

        return Database.raw(query, [latitude, longitude, latitude, radius]);

    }

}

module.exports = Flood
