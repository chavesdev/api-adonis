'use strict'

class StoreFlood {
  get rules () {
    return {
      title : 'required|string',
      latitude : 'required|number',
      longitude : 'required|number'
    }
  }

  get messages () {
    return {
      'latitude.required' : 'Informe a latitude',
      'longitude.required' : 'Informe a longitude'
    }
  }
}

module.exports = StoreFlood
