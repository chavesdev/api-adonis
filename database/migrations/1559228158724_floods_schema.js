'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FloodsSchema extends Schema {
  up () {
    this.create('floods', (table) => {
      table.increments()
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('title').nullable()
      table.string('desc').nullable()
      table.decimal('latitude',9, 6).notNullable()
      table.decimal('longitude',9, 6).notNullable()
      table.text('picture').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('floods')
  }
}

module.exports = FloodsSchema
